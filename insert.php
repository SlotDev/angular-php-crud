<?php 
  //insert.php
  
  include("connect_db.php");

  $form_data = json_decode(file_get_contents("php://input"));

  $error = '';
  $message = '';
  $validation_error = '';
  $first_name = '';
  $last_name = '';

  if($form_data->action == 'fetch_single_data'){

    $query = "SELECT * FROM tbl_sample WHERE id = '".$form_data->id."'";
    $stmt = $conn->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll();
    foreach ($result as $key => $row) {
      $output['first_name'] = $row['first_name'];
      $output['last_name'] = $row['last_name'];
    }

  }elseif($form_data->action == 'Delete'){

    $query = "DELETE FROM tbl_sample WHERE id = '".$form_data->id."'";
    $stmt = $conn->prepare($query);
    if($stmt->execute()){
      $output['message'] = 'Data Deleted';
    }

  }else{

    if(empty($form_data->first_name)){
      $error[] = 'First Name is Required';
    }else{
      $first_name = $form_data->first_name;
    }

    if(empty($form_data->last_name)){
      $error[] = 'Last Name is Required';

    }else{
      $last_name = $form_data->last_name;
    }
    
    if(empty($error)){

      if($form_data->action == 'Insert'){
        $data = array(
          ':first_name' =>  $first_name,
          ':last_name'  =>  $last_name
        );

        $query = "INSERT INTO tbl_sample (first_name, last_name) VALUES (:first_name, :last_name)";
        $stmt = $conn->prepare($query);
        $stmt->bindParam(':first_name', $first_name);
        $stmt->bindParam(':last_name', $last_name);
        if($stmt->execute()){
          // $output['message'] = 'Data Inserted';
          $message = 'Data Inserted';
        }
      }

      if($form_data->action == 'Edit'){
        $data = array(
          ':first_name' =>  $first_name,
          ':last_name'  =>  $last_name,
          ':id'         =>  $form_data->id
        );
        $query = "UPDATE tbl_sample SET first_name = :first_name, last_name = :last_name WHERE id = :id";
        $stmt = $conn->prepare($query);
        if($stmt->execute($data)){
          // $output['message'] = 'Data Edited';
          $message = 'Data Edited';
        }
      }

    }else{
      $validation_error = implode(", ", $error);
    }

    $output = array(
      'error' => $validation_error, 
      'message' => $message,
    );

  }
 

  echo json_encode($output);

 ?>